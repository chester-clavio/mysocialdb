const mongoose = require("mongoose")
const PostSchema = new mongoose.Schema({
    username:{type:String,required:true},
    text:{type:String},
    imgurl:{type:String},
    likes:[{username:String}],
    comments:[{username:String,comment:String}]
})

const model = mongoose.model("posts",PostSchema)
module.exports = model