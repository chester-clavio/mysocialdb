const mongoose = require("mongoose")
const UserSchema = new mongoose.Schema({
    username:{type:String, unique:true,required:true},
    password:{type:String,required:true},
    firstname:{type:String},
    lastname:{type:String}
})

const model = mongoose.model("users",UserSchema)
module.exports = model