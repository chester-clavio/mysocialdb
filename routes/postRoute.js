const express= require('express')
const router = express.Router()
const {createPost,getPosts,likePost,commentOnPost}= require('./../controllers/post')

router.post('/',(req,res)=>createPost(req,res))

router.get('/',(req,res)=>getPosts(req,res))

router.post('/comment/:id',(req,res)=>commentOnPost(req,res))

router.post('/like/:id',(req,res)=>likePost(req,res))

module.exports = router