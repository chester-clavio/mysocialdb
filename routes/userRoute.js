const express= require('express')
const router = express.Router()
const {createAccount,login} = require('../controllers/user')

router.post('/login',(req,res)=>login(req,res))

router.post('/',(req,res)=>createAccount(req,res))

module.exports = router