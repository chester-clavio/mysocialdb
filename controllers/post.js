const Post = require('./../models/Post')

module.exports.createPost = async (req,res)=>{
	let {username,text,imgurl} = req.body

	let newPost = new Post({
		username:username,
		text:text,
		imgurl:imgurl
	})
	try{
		newPost.save().then(result=>{
			res.json({status:"ok"})
		})
	}catch(e){
		res.json({status:'error',error:e})
	}

}

module.exports.getPosts= async (req,res)=>{
	Post.find().then(result=>res.send(result))
}

module.exports.likePost=async(req,res)=>{
	
	
	res.json({status:"ok",message:"test success",id:req.params.id})
}

module.exports.commentOnPost=async(req,res)=>{
	let postId = req.params.id.slice(3)
	let comment = req.body.comment
	let username = req.body.username
	let comArr =""
	Post.updateOne({_id:postId},{$push:{comments:{username,comment}}}).then(result=>{
		res.json({status:"ok"})
	})
}