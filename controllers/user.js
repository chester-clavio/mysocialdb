const User = require('./../models/User')
const CryptoJS= require('crypto-js')
const {createToken,verifyToken}= require('../auth')

module.exports.createAccount= async (req,res)=>{
    let {username,password} = req.body
    let newUser = new User({
        username:username,
        password:CryptoJS.AES.encrypt(password,process.env.SECRETKEY).toString()
    })
    try{
        await newUser.save().then(result=>{
            res.json({status:"ok"})
        })
    }catch(e){
        res.json({status:"error",error:e})
    }
}

module.exports.login= async(req,res)=>{
    let {username,password} = req.body
    if(username===undefined||password===undefined){
        res.json({status:"error",error:"No Input Detected"})
    }else{
        await User.findOne({username:username}).then(result=>{
            if(result===null){
                res.json({status:"error",error:"username not registered"})
            }else{
                if(password===CryptoJS.AES.decrypt(result.password,process.env.SECRETKEY).toString(CryptoJS.enc.Utf8)){
                    let token = createToken({
                       username:result.username
                    })
                    res.json({status:"ok",token:token})

                }else{
                    res.json({status:"error",error:"Wrong password"})
                }
            }
        })
    }
}