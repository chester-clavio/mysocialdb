require('dotenv').config()
const express = require('express')
const port = process.env.PORT||3009
const app = express()
const cors= require('cors')

//routes
app.use(cors())
app.use(express.json())
const userRoute= require('./routes/userRoute')
app.use('/api/users',userRoute)

const postRoutes=require('./routes/postRoute')
app.use('/api/posts',postRoutes)

//connect to db
const mongoose = require('mongoose')
mongoose.connect(process.env.MONGO)
const db = mongoose.connection

db.on('open',()=> console.log("Connected to database"))
db.on('error',console.error.bind(console,"database error"))

app.listen(port,()=>console.log('Server started on port',port))